# Develoment dependencies
# -----------------------

black

flake8-builtins>=1.5
flake8-isort>=6.1
flake8-qgis>=1

pre-commit>=3.7,<4
