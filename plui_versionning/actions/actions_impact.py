import datetime
import json
import time

import actions_versionning as av
from qgis.core import QgsLayerTreeLayer, QgsProject, QgsRectangle, QgsVectorLayer


def afficher_impacts(qgis, feature):
    #    try :

    # Récupération du groupe "IMPACTS"
    rootNode = QgsProject.instance().layerTreeRoot().findGroup("IMPACTS")

    # Création si besoin d'un sous-groupe de résultats pour l'impact sélectionné
    impactInstance = rootNode.findGroup(
        'Résultats impact "' + feature["intitule"] + '"'
    ) or rootNode.addGroup('Résultats impact "' + feature["intitule"] + '"')

    # Création d'un sous-groupe contenant les résultats à la date
    results = impactInstance.addGroup(
        datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    )

    # Initialisation de la bbox de zoom
    extent = QgsRectangle()
    extent.setMinimal()

    start = time.time()

    for layer in json.loads(feature["impact"]):
        startloading = time.time()
        # Chargement de la couche dans le groupe de résultats
        vlayer = QgsVectorLayer(
            """
            service='{service}'
            sslmode=disable
            key='{key}'
            table="{schemaname}"."{tablename}" ({geom})
            sql="{key}" IN ({ids})
            """.format(
                service=av.service_conf,
                schemaname=layer["couche"].split(".")[0],
                tablename=layer["couche"].split(".")[1],
                ids=layer["objets"]["id"][
                    1:-1
                ],  # Manière plus propre via un tableau d'identifiants : ', '.join([str(v) for v in layer['objets']['id']]),
                key=layer["id"],
                geom=layer["geom_col"],
            ),
            layer["couche"].split(".")[1],
            "postgres",
        )

        QgsProject.instance().addMapLayer(vlayer, False)

        results.insertChildNode(1, QgsLayerTreeLayer(vlayer))

        # Prise en compte de la bbox de la couche
        extent.combineExtentWith(vlayer.extent())

        print(
            'loading time for layer "',
            layer["couche"].split(".")[1],
            '" : ',
            time.time() - startloading,
        )

    # Zoom sur les éléments impactés
    qgis.utils.iface.mapCanvas().setExtent(extent)
    qgis.utils.iface.mapCanvas().refresh()

    print("end:", time.time() - start)
