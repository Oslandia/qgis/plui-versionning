-- ##########################################################################################################################################
-- vue de travail des objets
-- ##########################################################################################################################################

-- DEPLOYEMENT AUTOMATIQUE avec la fonction admin_deployer_vues_entite

SELECT urba_plui_ecriture.admin_deployer_vues_entite('zoneurba');

SELECT urba_plui_ecriture.admin_deployer_vues_entite('prescription');

SELECT urba_plui_ecriture.admin_deployer_vues_entite('information');

SELECT urba_plui_ecriture.admin_deployer_vues_entite('habillage');
