#!/bin/bash

GNUGETOPT="getopt"
if [[ "$OSTYPE" =~ FreeBSD* ]] || [[ "$OSTYPE" =~ darwin* ]]; then
	GNUGETOPT="/usr/local/bin/getopt"
elif [[ "$OSTYPE" =~ openbsd* ]]; then
	GNUGETOPT="gnugetopt"
fi

# Exit on error
set -e
set -u

# Default values
PGHOST=localhost
PGPORT=5432
PGDATABASE=database
PGUSER=postgres
WITH_DATA=0
CREATE=0

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Menu
while [[ $# > 0 ]]; do
key="$1"
case $key in
    -h|--help)
        echo "Arguments:"
        echo -e "\t-h|--host\t\tNom d'hôte du serveur où la base de données se situe"
        echo -e "\t-p|--port\t\tPort de connexion à la base de données"
        echo -e "\t-d|--dbname\t\tNom de la base de données"
        echo -e "\t-U|--username\t\tUtilisateur de la base de données"
        echo -e "\t-D|--data\t\t\tIntègre les données de test à la base de données"
        echo -e "\t-C|--create\t\tSupprime et crée la base de données et installe les extensions nécessaires"
        echo
        echo -e "Exemple: "
        echo -e "\t./create_plui_db.sh -h localhost -p 5432 -d plui -U postgres"
        echo
        exit 0
    ;;
    -h|--host)
        PGHOST="$2"
        shift # past argument
    ;;
    -p|--port)
        PGPORT="$2"
        shift # past argument
    ;;
    -d|--dbname)
        PGDATABASE="$2"
        shift # past argument
    ;;
    -U|--username)
        PGUSER="$2"
        shift # past argument
    ;;
    -D|--with_data)
        WITH_DATA=1
    ;;
    -C|--create)
        CREATE=1
esac
shift
done

echo "Paramètres:"
printf "\t${GREEN}PGHOST        = ${PGHOST}${NC}\n"
printf "\t${GREEN}PGPORT        = ${PGPORT}${NC}\n"
printf "\t${GREEN}PGDATABASE    = ${PGDATABASE[*]}${NC}\n"
printf "\t${GREEN}PGUSER        = ${PGUSER}${NC}\n"
printf "\t${GREEN}CREATE        = ${CREATE}${NC}\n"
printf "\t${GREEN}WITH_DATA     = ${WITH_DATA}${NC}\n"
echo

# Vérifier si variable ENV PGCLIENTENCODING=UTF8

if [[ $CREATE -eq 1 ]]; then
    printf "\n${BLUE}Suppression de la base de données ${PGDATABASE}${NC}\n\n"
    dropdb --if-exists -U $PGUSER -h ${PGHOST} -p ${PGPORT} ${PGDATABASE}
    printf "\t${GREEN}Base de données $PGDATABASE supprimée${NC}\n"

    printf "\n${BLUE}Création de la base de données ${PGDATABASE}${NC}\n\n"
    createdb -U $PGUSER -h ${PGHOST} -p ${PGPORT} ${PGDATABASE}
    printf "\t${GREEN}Base de données $PGDATABASE créée${NC}\n"

    printf "\n${BLUE}Installation des extensions postgis et hstore${NC}\n\n"
    psql -h $PGHOST -p $PGPORT -d $PGDATABASE -U $PGUSER \
        -c 'CREATE EXTENSION IF NOT EXISTS postgis;' \
        -c 'CREATE EXTENSION IF NOT EXISTS postgis_topology;' \
        -c 'CREATE EXTENSION IF NOT EXISTS hstore;'
    printf "\t${GREEN}Extensions installées${NC}\n"
fi

psql -h $PGHOST -p $PGPORT -d $PGDATABASE -U $PGUSER \
    -c "BEGIN TRANSACTION;" \
    -c "SET client_min_messages TO ERROR;" \
    -f 01_mcd_cnig.sql \
    -f 02_mcd_plugin.sql \
    -f 03_sequences.sql \
    -f 05_fonctions_admin.sql \
    -f 06_fonctions_trigger_plugin.sql \
    -f 07_fonctions_trigger_cnig.sql \
    -f 08_fonctions_zoneurba.sql \
    -f 09_fonctions_prescription.sql \
    -f 10_fonctions_information.sql \
    -f 11_fonctions_habillage.sql \
    -f 12_fonctions_coeur_versionning.sql \
    -f 13_integration_donnees_ref_cnig.sql \
    -f 14_integration_donnees_ref_ct.sql \
    -f 16_vues_cnig.sql \
    -f 17_vues_plugin.sql \
    -c "COMMIT;"

if [[ $WITH_DATA -eq 1 ]]; then
    psql -h $PGHOST -p $PGPORT -d $PGDATABASE -U $PGUSER \
        -f 15_integration_jeu_donnees.sql
fi
