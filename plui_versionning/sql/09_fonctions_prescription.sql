-- les fonctions de création, modification et supprission des PRESCRIPTIONS
-- ATTENTION : dans cette fonction le type et sous-type de la prescription sont remplis à partir du sous-sous type renseigné dans le projet QGIS
-- la liste des sous-sous type est issue de la table prescriptionurbatype

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_creer_prescription(tr hstore, geomtype character)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
    hstorelib      hstore;
    next_id_classe bigint;
    next_seq_value bigint;
    loaded_version character varying(20);
    insee          text; -- Liste des codes INSEE localisant l'entité à enregistrer
    id             text; -- stockage identifiant retourné par insert_into_from_hstore
    urlfic         TEXT;

BEGIN

    --
    -- Remplissage des champs à recaler
    --

    -- Récupération des prochains numéros de serial
    next_id_classe := nextval('urba_plui_ecriture.prescription_idcnigprescription_seq'::regclass);
    next_seq_value := nextval('urba_plui_ecriture.prescription_idprescription_seq'::regclass);

    -- changer le champ lib_modif à I pour insertion
    tr := tr || (
        SELECT ('lib_modif=>' || 'I')::public.hstore);

    -- Création ou mise à jour de l'identifiant unique (si c'est une copie d'entité depuis la couche des entités supprimées)
    tr := tr || (
        SELECT ('idprescription=>' || next_seq_value::text)::public.hstore);

    -- Constitution de l'identifiant de classe à partir de la séquence de PROD
    tr := tr || (
        SELECT ('lib_idpsc=>' || concat(geomtype, next_id_classe::text))::public.hstore);

    RAISE NOTICE 'TR avant lib_typepsc_ct : %', tr;
    -- Remplissage des champs type et sous-type de prescription à partir du type spécifique à la collectivité (s'il existe)
    IF (tr -> 'lib_typepsc_ct')::TEXT IS NOT NULL
        THEN
            tr := tr || (
                SELECT ('typepsc=>' || split_part(tr -> 'lib_typepsc_ct', '-', 1)::text)::public.hstore);
            tr := tr || (
                SELECT ('stypepsc=>' || split_part(tr -> 'lib_typepsc_ct', '-', 2)::text)::public.hstore);
    END IF;


    RAISE NOTICE 'TR : %', tr;

    -- Remplissage du champ lib_code_insee
    IF geomtype = 'PP' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_pct'));
    ELSIF geomtype = 'PL' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_lin'));
    ELSIF geomtype = 'PS' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_surf'));
    ELSE
        RAISE EXCEPTION 'Type de géométrie % inconnu', geomtype;
    END IF;

    tr := tr || (
        SELECT ('lib_code_insee=>' || quote_ident(insee))::public.hstore);

    -- Remplissage du champ urlfic
    -- FIXME : bloqué car non essentiel pour le versionning
--    SELECT lib_urlfic INTO urlfic
--    FROM urba_plui_ecriture.prescriptionurbatype
--    WHERE lib_procedure_plui = tr -> 'lib_procedure_plui'
--        AND lib_sstypepsc = tr -> 'lib_sstypepsc';
--
--    IF urlfic IS NULL
--        THEN RAISE EXCEPTION 'Le champ "lib_url" de la table prescriptionurbatype n''est pas renseigné pour le code sous-sous type "%"', tr -> 'lib_sstypepsc';
--    ELSE
--        tr := tr || (
--            SELECT ('urlfic=>' || quote_ident(urlfic))::public.hstore);
--    END IF;
    --raise notice 'entité après MAJ du champ urlfic : %', tr::text;

    --
    -- Enregistrement des données
    --

    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'prescription'));

    -- Retrait des champs "lib_" depuis le hstore des colonnes fixes
    tr := delete(tr, hstorelib);

    --raise notice 'tr : %', tr::text;
    --raise notice 'hstorelib : %', hstorelib::text;

    -- insertion dans la prescription
    PERFORM urba_plui_ecriture.func_creer_avec_hstore('urba_plui_ecriture', 'prescription', tr);

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.prescription
    SET lib_hstore = hstorelib
    WHERE idprescription = next_seq_value;

END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_creer_prescription(hstore,bpchar) IS 'Insère une nouvelle entité dans la table de prescription';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_modifier_prescription(old hstore, new hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    hstorelib hstore;
    insee          text; -- Liste des codes INSEE localisant l'entité à enregistrer
    urlfic         TEXT;

BEGIN

    --
    -- Remplissage des champs à recaler
    --

    IF old -> 'lib_modif' != 'I'
        -- changer le champ lib_modif à M pour modification
    THEN
        new := new || (
            SELECT ('lib_modif=>' || 'M')::public.hstore);
    END IF;

    -- Remplissage du champ lib_code_insee
    IF exist(new,'geom_pct') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_pct'));
    ELSIF exist(new,'geom_lin') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_lin'));
    ELSIF exist(new,'geom_surf') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_surf'));
    ELSE
        RAISE EXCEPTION 'Type de géométrie % inconnu', geomtype;
    END IF;

    -- Mise à jour du sous-type de prescription à partir du type spécifique à la collectivité
    IF (new -> 'lib_typepsc_ct')::TEXT IS NOT NULL
        THEN
            new := new || (
                SELECT ('stypepsc=>' || split_part(new -> 'lib_typepsc_ct', '-', 2)::text)::public.hstore);
    END IF;

    -- Remplissage du champ urlfic
    -- FIXME : bloqué car non essentiel pour le versionning
--    SELECT lib_urlfic INTO urlfic
--    FROM urba_plui_ecriture.prescriptionurbatype
--    WHERE lib_procedure_plui = new -> 'lib_procedure_plui'
--        AND lib_sstypepsc = new -> 'lib_sstypepsc';
--
--    IF urlfic IS NULL
--        THEN RAISE EXCEPTION 'Le champ "lib_url" de la table prescriptionurbatype n''est pas renseigné pour le code sous-sous type "%"', new -> 'lib_sstypepsc';
--    ELSE
--        new := new || (
--            SELECT ('urlfic=>' || quote_ident(urlfic))::public.hstore);
--    END IF;
--    raise notice 'entité après MAJ du champ urlfic : %', new::text;

    -- Récupération des champs "lib_"
    hstorelib := slice(new, array(SELECT nom_col
                                  FROM urba_plui_ecriture.pluiv_champs_lib
                                  WHERE classe_entite = 'prescription'));

    -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
    new := delete(new, hstorelib);

    -- Mise à jour du zonage dans la table
    PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'prescription', new, 'idprescription',
                                                new -> 'idprescription');

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.prescription
    SET lib_hstore = hstorelib
    WHERE idprescription = (new -> 'idprescription')::bigint;

END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_modifier_prescription(hstore,hstore) IS 'Change le statut de l''entité  à ''''M'''' en même temps que la modification des autres éléments de l''entité';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_supprimer_prescription(tr hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    hstorelib hstore;
BEGIN
    -- changer le champ lib_modif à S pour suppression
    tr := tr || (
        SELECT ('lib_modif=>' || 'S')::public.hstore);

    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'prescription'));

    -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
    tr := delete(tr, hstorelib);

    -- Mise à jour du zonage dans la table
    PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'prescription', tr, 'idprescription',
                                                tr -> 'idprescription');

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.prescription
    SET lib_hstore = hstorelib
    WHERE idprescription = (tr -> 'idprescription')::bigint;
END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_supprimer_prescription(hstore) IS 'Change le statut de la zone à ''S'' sans supprimer l''entité de la table';
