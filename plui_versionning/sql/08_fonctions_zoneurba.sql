-- les fonctions de création, modification et supprission du ZONAGE

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_creer_zoneurba(tr hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
    hstorelib      hstore;
    next_id_classe bigint;
    next_seq_value bigint;
    insee    text;
    id             text; -- stockage identifiant retourné par insert_into_from_hstore

BEGIN

    -- Récupération des prochains numéros de serial
    next_id_classe := nextval('urba_plui_ecriture.zoneurba_idcnigzoneurba_seq'::regclass);
    next_seq_value := nextval('urba_plui_ecriture.zoneurba_idzoneurba_seq'::regclass);

    -- changer le champ lib_modif à I pour insertion
    tr := tr || (
        SELECT ('lib_modif=>' || 'I')::public.hstore);

    -- Création ou mise à jour de l'identifiant unique (si c'est une copie d'entité depuis la couche des entités supprimées)
    tr := tr || (
        SELECT ('idzoneurba=>' || next_seq_value::text)::public.hstore);

    -- Constitution de l'identifiant de classe à partir de la séquence dédiée
    tr := tr || (
        SELECT ('lib_idzone=>' || concat('ZO', next_id_classe::text))::public.hstore);

    -- Remplissage du champ lib_code_insee
    insee = (
        SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom'));

    tr := tr || (
        SELECT ('lib_code_insee=>' || quote_ident(insee))::public.hstore);


    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'zoneurba'));

    -- Retrait des champs "lib_" depuis le hstore des colonnes fixes
    tr := delete(tr, hstorelib);

    --raise notice 'tr : %', tr::text;
    --raise notice 'hstorelib : %', hstorelib::text;

    -- Enregistrement des colonnes fixes
    PERFORM urba_plui_ecriture.func_creer_avec_hstore('urba_plui_ecriture', 'zoneurba', tr);

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.zoneurba
    SET lib_hstore = hstorelib
    WHERE idzoneurba = next_seq_value;


END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_creer_zoneurba(hstore) IS 'Insère une nouvelle zone dans la table de zonage';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_modifier_zoneurba(old hstore, new hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
    hstorelib hstore;
    insee          text; -- Liste des codes INSEE localisant l'entité à enregistrer
BEGIN
    IF old -> 'lib_modif' = 'N'
        -- changer le champ lib_modif à M pour modification
    THEN
        new := new || (
            SELECT ('lib_modif=>' || 'M')::public.hstore);

    END IF;

    -- Ne peut s'appliquer sur des objets supprimés d'une version
    IF old -> 'lib_modif' != 'S'
    THEN

            -- Remplissage du champ lib_code_insee
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom'));

        new := new || (
            SELECT ('lib_code_insee=>' || quote_ident(insee))::public.hstore);

        -- Récupération des champs "lib_"
        hstorelib := slice(new, array(SELECT nom_col
                                      FROM urba_plui_ecriture.pluiv_champs_lib
                                      WHERE classe_entite = 'zoneurba'));



        -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
        new := delete(new, hstorelib);

        -- Mise à jour du zonage dans la table
        PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'zoneurba', new, 'idzoneurba',
                                                    new -> 'idzoneurba');

        -- Enregistrement de la colonne hstore contenant les champs "lib_"
        UPDATE urba_plui_ecriture.zoneurba
        SET lib_hstore = hstorelib
        WHERE idzoneurba = (new -> 'idzoneurba')::bigint;
    ELSE
        RAISE EXCEPTION 'Zone déjà supprimée !';
    END IF;


END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_modifier_zoneurba(hstore,hstore) IS 'Change le statut de l''entité à "M" en même temps que la modification des autres éléments de l''entité';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_supprimer_zoneurba(tr hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
    hstorelib hstore;
BEGIN
    -- changer le champ lib_modif à S pour suppression
    tr := tr || (
        SELECT ('lib_modif=>' || 'S')::public.hstore);

    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'zoneurba'));

    -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
    tr := delete(tr, hstorelib);

    -- pas de Mise à jour du zonage dans la table nécessaire (traité dans le libhstore)
    PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'zoneurba', tr, 'idzoneurba', tr -> 'idzoneurba');

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.zoneurba
    SET lib_hstore = hstorelib
    WHERE idzoneurba = (tr -> 'idzoneurba')::bigint;
END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_supprimer_zoneurba(hstore) IS 'Change le statut de l''entité à ''S'' sans supprimer l''entité de la table';
