--


-- ZONAGE

CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_zoneurba()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

    editable       boolean;
    loaded_version int;

BEGIN

    IF TG_OP = 'INSERT' THEN

        -- Récupération de la version de contexte
        -- FIXME : problème pour récupérer le contexte de la version numérisée
--        SELECT current_version
--        INTO loaded_version
--        FROM urba_plui_ecriture.pluiv_contexte_utilisateur
--        WHERE user_logged = current_user
--        AND classe_entite = split_part(TG_TABLE_NAME, '_', 2);

        -- Recalage de la version dans laquelle l'entité est insérée (cas de récupération d'une entité copiée depuis une autre version)
--        NEW. = loaded_version;
--        NEW.lib_id_version = (SELECT id FROM urba_plui_ecriture.v_pluiv_versions WHERE id_version = NEW.idurba);

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = loaded_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_creer_zoneurba(hstore(NEW));

    ELSIF TG_OP = 'UPDATE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = NEW.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_modifier_zoneurba(hstore(OLD), hstore(NEW));

    ELSIF TG_OP = 'DELETE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = OLD.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_supprimer_zoneurba(hstore(OLD));

    END IF;

    RETURN NEW;

END;

$function$
;

-- INFORMATION

CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_information()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
    editable       boolean;
    loaded_version int;

BEGIN

    IF TG_OP = 'INSERT' THEN

        -- Récupération de la version de contexte
        -- FIXME : problème pour récupérer le contexte de la version numérisée
--        SELECT current_version
--        INTO loaded_version
--        FROM urba_plui_ecriture.pluiv_contexte_utilisateur
--        WHERE user_logged = current_user
--        AND classe_entite = split_part(TG_TABLE_NAME, '_', 2);

        -- Recalage de la version dans laquelle l'entité est insérée (cas de récupération d'une entité copiée depuis une autre version)
--        NEW. = loaded_version;
--        NEW.lib_id_version = (SELECT id FROM urba_plui_ecriture.v_pluiv_versions WHERE id_version = NEW.idurba);

        -- Vérification du statut de la version à mettre à jour
        SELECT urba_plui_ecriture.func_verifier_statut_modifiable(loaded_version)
        INTO editable;
        IF NOT editable THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', loaded_version;
        END IF;

        -- Vérification sur l'intégrité de l'entité à enregistrer
        --PERFORM urba_plui_ecriture.func_verifier_champs_entite(hstore(NEW), 'I');

        PERFORM urba_plui_ecriture.qgis_creer_information(hstore(NEW), TG_ARGV[0]);

    ELSIF TG_OP = 'UPDATE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = NEW.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        -- Vérification sur l'intégrité de l'entité à enregistrer
        --PERFORM urba_plui_ecriture.func_verifier_champs_entite(hstore(NEW), 'I');

        PERFORM urba_plui_ecriture.qgis_modifier_information(hstore(OLD), hstore(NEW));

    ELSIF TG_OP = 'DELETE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = OLD.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_supprimer_information(hstore(OLD));

    END IF;

    RETURN NEW;
END;
$function$
;

-- PRESCRIPTION

CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_prescription()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
    editable       boolean;
    loaded_version int;

BEGIN

    IF TG_OP = 'INSERT' THEN
        -- Récupération de la version de contexte
        -- FIXME : problème pour récupérer le contexte de la version numérisée
--        SELECT current_version
--        INTO loaded_version
--        FROM urba_plui_ecriture.pluiv_contexte_utilisateur
--        WHERE user_logged = current_user
--        AND classe_entite = split_part(TG_TABLE_NAME, '_', 2);

        -- Recalage de la version dans laquelle l'entité est insérée (cas de récupération d'une entité copiée depuis une autre version)
--        NEW. = loaded_version;
--        NEW.lib_id_version = (SELECT id FROM urba_plui_ecriture.v_pluiv_versions WHERE id_version = NEW.idurba);

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = loaded_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        -- Vérification sur l'intégrité de l'entité à enregistrer
        --PERFORM urba_plui_ecriture.func_verifier_champs_entite(hstore(NEW), 'P'::bpchar);

        PERFORM urba_plui_ecriture.qgis_creer_prescription(hstore(NEW), TG_ARGV[0]);

    ELSIF TG_OP = 'UPDATE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = NEW.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        -- Vérification sur l'intégrité de l'entité à enregistrer
        --PERFORM urba_plui_ecriture.func_verifier_champs_entite(hstore(NEW), 'P'::bpchar);

        PERFORM urba_plui_ecriture.qgis_modifier_prescription(hstore(OLD), hstore(NEW));

    ELSIF TG_OP = 'DELETE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = OLD.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_supprimer_prescription(hstore(OLD));

    END IF;

    RETURN NEW;
END;
$function$
;

-- HABILLAGE

CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_habillage()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE

    editable       boolean;
    loaded_version character varying(20);

BEGIN

    IF TG_OP = 'INSERT' THEN

        -- Récupération de la version de contexte
        -- FIXME : problème pour récupérer le contexte de la version numérisée
--        SELECT current_version
--        INTO loaded_version
--        FROM urba_plui_ecriture.pluiv_contexte_utilisateur
--        WHERE user_logged = current_user
--        AND classe_entite = split_part(TG_TABLE_NAME, '_', 2);

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = loaded_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        -- Recalage de la version dans laquelle l'entité est insérée (cas de récupération d'une entité copiée depuis une autre version)
        NEW.lib_id_version = loaded_version;
        NEW.idurba = (SELECT idurba FROM urba_plui_ecriture.pluiv_versions WHERE id = loaded_version);

        PERFORM urba_plui_ecriture.qgis_creer_habillage(hstore(NEW), TG_ARGV[0]);

    ELSIF TG_OP = 'UPDATE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = NEW.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_modifier_habillage(hstore(OLD), hstore(NEW));

    ELSIF TG_OP = 'DELETE' THEN

        -- Vérification du statut de la version à mettre à jour
        IF NOT (
            SELECT s.editable
            FROM urba_plui_ecriture.pluiv_versions v
            JOIN urba_plui_ecriture.pluiv_versionlibellestatut s ON v.statut = s.valeur
            WHERE v.id = OLD.lib_id_version
        )
        THEN
            RAISE EXCEPTION 'La version "%" n''est pas éditable.', NEW.idurba;
        END IF;

        PERFORM urba_plui_ecriture.qgis_supprimer_habillage(hstore(OLD));

    END IF;

    RETURN NEW;

END;
$function$
;
