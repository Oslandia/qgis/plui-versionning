-- Sequences en plus

-- séquence qui permettent de repérer un objet identique entre les procédures
-- par exemple : un zonnage portera le même id sur plusieurs versions et procédure
-- cela permet de repérer un même objet au sens "juridique"

-- ATTENTION : les "START" sont "réglés" pour la GAM

-- DROP SEQUENCE urba_plui_ecriture.zoneurba_idcnigzoneurba_seq;

CREATE SEQUENCE urba_plui_ecriture.zoneurba_idcnigzoneurba_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 31572
    CACHE 1
    NO CYCLE;

-- DROP SEQUENCE urba_plui_ecriture.prescription_idcnigprescription_seq;

CREATE SEQUENCE urba_plui_ecriture.prescription_idcnigprescription_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 238922
    CACHE 1
    NO CYCLE;

-- DROP SEQUENCE urba_plui_ecriture.information_idcniginformation_seq;

CREATE SEQUENCE urba_plui_ecriture.information_idcniginformation_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 628556
    CACHE 1
    NO CYCLE;

-- DROP SEQUENCE urba_plui_ecriture.habillage_idcnighabillage_seq;

CREATE SEQUENCE urba_plui_ecriture.habillage_idcnighabillage_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 34000
    CACHE 1
    NO CYCLE;
