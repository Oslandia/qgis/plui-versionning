-- les fonctions de création, modification et supprission de l'HABILLAGE

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_creer_habillage(tr hstore, geomtype character)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    hstorelib      hstore;
    next_id_classe bigint;
    next_seq_value bigint;
    loaded_version character varying(20);
    insee          text; -- Liste des codes INSEE localisant l'entité à enregistrer
    id             text; -- stockage identifiant retourné par insert_into_from_hstore

BEGIN

    -- Récupération des prochains numéros de serial
    next_id_classe := nextval('urba_plui_ecriture.habillage_idcnighabillage_seq'::regclass);
    next_seq_value := nextval('urba_plui_ecriture.habillage_idhabillage_seq'::regclass);

    -- changer le champ lib_modif à I pour insertion
    tr := tr || (
        SELECT ('lib_modif=>' || 'I')::public.hstore);

    -- Création ou mise à jour de l'identifiant unique (si c'est une copie d'entité depuis la couche des entités supprimées)
    tr := tr || (
        SELECT ('idhabillage=>' || next_seq_value::text)::public.hstore);

    -- Constitution de l'identifiant de classe à partir de la séquence de PROD
    tr := tr || (
        SELECT ('lib_idhab=>' || concat(geomtype, next_id_classe::text))::public.hstore);

    -- Remplissage du champ lib_code_insee
    IF geomtype = 'HP' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_pct'));
    ELSIF geomtype = 'HL' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_lin'));
    ELSIF geomtype = 'HS' THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(tr -> 'geom_surf'));
    ELSE
        RAISE EXCEPTION 'Type de géométrie % inconnu', geomtype;
    END IF;

    tr := tr || (
        SELECT ('lib_code_insee=>' || quote_ident(insee))::public.hstore);

    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'habillage'));

    -- Retrait des champs "lib_" depuis le hstore des colonnes fixes
    tr := delete(tr, hstorelib);

    --raise notice 'tr : %', tr::text;
    --raise notice 'hstorelib : %', hstorelib::text;

    -- insertion dans la table habillage
    SELECT urba_plui_ecriture.func_creer_avec_hstore('urba_plui_ecriture', 'habillage', tr)
    INTO id;

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.habillage
    SET lib_hstore = hstorelib
    WHERE idhabillage = next_seq_value;

END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_creer_habillage(hstore,bpchar) IS 'Insère une nouvelle entité dans la table de habillage';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_modifier_habillage(old hstore, new hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
    hstorelib hstore;
    insee          text; -- Liste des codes INSEE localisant l'entité à enregistrer
BEGIN
    IF old -> 'lib_modif' != 'I'
        -- changer le champ lib_modif à M pour modification
    THEN
        new := new || (
            SELECT ('lib_modif=>' || 'M')::public.hstore);
    END IF;

    -- Remplissage du champ lib_code_insee
    IF exist(new,'geom_pct') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_pct'));
    ELSIF exist(new,'geom_lin') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_lin'));
    ELSIF exist(new,'geom_surf') THEN
        insee = (
            SELECT urba_plui_ecriture.func_recuperer_code_insee(new -> 'geom_surf'));
    ELSE
        RAISE EXCEPTION 'Type de géométrie % inconnu', geomtype;
    END IF;

    -- Récupération des champs "lib_"
    hstorelib := slice(new, array(SELECT nom_col
                                  FROM urba_plui_ecriture.pluiv_champs_lib
                                  WHERE classe_entite = 'habillage'));

    -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
    new := delete(new, hstorelib);

    -- Mise à jour du zonage dans la table
    PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'habillage', new, 'idhabillage',
                                                new -> 'idhabillage');

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.habillage
    SET lib_hstore = hstorelib
    WHERE idhabillage = (new -> 'idhabillage')::bigint;

END;

$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_modifier_habillage(hstore,hstore) IS 'Change le statut de l''entité  à ''''M'''' en même temps que la modification des autres éléments de l''entité';

-- ###############################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.qgis_supprimer_habillage(tr hstore)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
    hstorelib hstore;
BEGIN
    -- changer le champ lib_modif à S pour suppression
    tr := tr || (
        SELECT ('lib_modif=>' || 'S')::public.hstore);

    -- Récupération des champs "lib_"
    hstorelib := slice(tr, array(SELECT nom_col
                                 FROM urba_plui_ecriture.pluiv_champs_lib
                                 WHERE classe_entite = 'habillage'));

    -- Suppression des champs "lib_" pour n'avoir que les colonnes fixes
    tr := delete(tr, hstorelib);

    -- Mise à jour du zonage dans la table
    PERFORM urba_plui_ecriture.func_maj_avec_hstore('urba_plui_ecriture', 'habillage', tr, 'idhabillage', tr -> 'idhabillage');

    -- Enregistrement de la colonne hstore contenant les champs "lib_"
    UPDATE urba_plui_ecriture.habillage
    SET lib_hstore = hstorelib
    WHERE idhabillage = (tr -> 'idhabillage')::bigint;

END;

$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.qgis_supprimer_habillage(hstore) IS 'Change le statut de la zone à ''S'' sans supprimer l''entité de la table';
