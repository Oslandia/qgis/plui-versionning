-- PLUI versionning

-- script qui permet la création de la structure du standard CNIG
-- Vous pouvez vous référer au standard CNIG 2017

-- Sur certaines tables, des champs sont ajoutés pour le versionning : les champs débutent par lib_ (=cnig) ou pluiv_ (=spécifique plugin)

-- Création du SCHEMA

DROP SCHEMA IF EXISTS urba_plui_ecriture CASCADE;

CREATE SCHEMA urba_plui_ecriture;

COMMENT ON SCHEMA urba_plui_ecriture IS 'Environnement de développement pour la numérisation du PLUi';

-- documenturba #############################################################################################################################
-- cette table est issue du CNIG mais n'est pas utilisée dans le plugin

-- DROP TABLE urba_plui_ecriture.documenturba;
CREATE TABLE urba_plui_ecriture.documenturba (
    idurba varchar(30) NOT NULL,
    typedoc varchar(4) NULL,
    "etat" varchar(2) NULL,
    dateappro date NULL,
    datefin date NULL,
    siren bpchar(9) NULL,
    nomreg varchar(80) NULL,
    nomplan varchar(80) NULL,
    urlplan varchar(254) NULL,
    siteweb varchar(254) NULL,
    typeref varchar(2) NULL,
    dateref date NULL,
    urlreg varchar(254) NULL,
    nomproc varchar(10) NULL,
    urlpe varchar(254) NULL,
    CONSTRAINT documenturba_pkey PRIMARY KEY (idurba)
);
COMMENT ON TABLE urba_plui_ecriture.documenturba IS 'Table listant les versions d''assemblage validées pour chaque phase de procédure';

-- DROP TABLE urba_plui_ecriture.documenturbatype;
CREATE TABLE urba_plui_ecriture.documenturbatype (
    id serial NOT NULL,
    typedoc TEXT NULL,
    lib_typedoc TEXT NULL,
    CONSTRAINT documenturbatype_pkey1 PRIMARY KEY (id),
    CONSTRAINT documenturbatype_unique UNIQUE (typedoc)
);
COMMENT ON TABLE urba_plui_ecriture.documenturbatype IS 'Table de référence des différents types de documents d''urbanisme';

-- fkey sur documenturba
ALTER TABLE urba_plui_ecriture.documenturba ADD
    CONSTRAINT documenturbatype_fk
    FOREIGN KEY (typedoc)
    REFERENCES urba_plui_ecriture.documenturbatype(typedoc) ON
        DELETE
            RESTRICT ON
            UPDATE
            CASCADE;


 -- DROP TABLE urba_plui_ecriture.etatdocumenttype;
CREATE TABLE urba_plui_ecriture.etatdocumenttype (
    id serial NOT NULL,
    etat TEXT NULL,
    lib_etat TEXT NULL,
    CONSTRAINT etatdocumenttype_pkey1 PRIMARY KEY (id),
    CONSTRAINT etatdocumenttype_unique UNIQUE (etat)
);

COMMENT ON TABLE urba_plui_ecriture.etatdocumenttype IS 'Table de référence des différents états d''un document d''urbanisme';

-- fkey sur documenturba
ALTER TABLE urba_plui_ecriture.documenturba ADD
    CONSTRAINT etatdocumenttype_fk
    FOREIGN KEY (etat)
    REFERENCES urba_plui_ecriture.etatdocumenttype(etat) ON
        DELETE
            RESTRICT ON
            UPDATE
            CASCADE;


-- DROP TABLE urba_plui_ecriture.procedureurbatype;
CREATE TABLE urba_plui_ecriture.procedureurbatype (
    id serial NOT NULL,
    nomproc varchar NULL,
    lib_nomproc varchar NULL,
    CONSTRAINT procedureurbatype_nomproc_unique UNIQUE (nomproc),
    CONSTRAINT procedureurbatype_pkey PRIMARY KEY (id)
);

COMMENT ON TABLE urba_plui_ecriture.procedureurbatype IS 'Table de référence des différents types de procédure d''un document d''urbanisme';



-- zoneurba #############################################################################################################################


-- DROP TABLE urba_plui_ecriture.zoneurba;

CREATE TABLE urba_plui_ecriture.zoneurba (
    idzoneurba serial NOT NULL, -- Identifiant unique de la zone¶
    idurba varchar(30) NULL, -- Identifiant du document d'urbanisme (détourné dans le plugin pour identifier la version)
    libelle varchar(12) NULL, -- Nom court de la zone tel qu'il apparaît sur le plan de zonage¶
    libelong varchar(254) NULL, -- Nom complet littéral de la zone tel qu'il apparaît dans le chapitre du règlement écrit¶
    typezone varchar(3) NULL, -- Type de la zone classé dans une nomenclature simplifiée
    formdomi char(4) NULL, -- Forme d'aménagement dominante souhaitée pour la zone afin de répondre aux besoins de réhabilitation, restructuration ou d'aménagement. Par exemple, une zone de type U peut voir sa forme urbaine dominante différer suivant qu'elle accueille préférentiellement tel type d'habitat ou d'équipement
    destoui varchar(120) NULL, -- Destinations et sous-destinations autorisées
    destcdt varchar(120) NULL, -- Destinations et sous-destinations conditionnées
    destnon varchar(120) NULL, -- Destinations et sous-destinations non autorisées
--    destoui _char NULL, -- Destinations et sous-destinations autorisées
--    destcdt _char NULL, -- Destinations et sous-destinations conditionnées
--    destnon _char NULL, -- Destinations et sous-destinations non autorisées
    nomfic varchar(80) NULL, -- Nom du fichier contenant le texte du règlement de la zone
    urlfic varchar(254) NULL, -- Lien d'accès au fichier contenant le texte du règlement de la zone ou à defaut du règlement intégral indexé
    datevalid date NULL, -- Date de la dernière validation de la zone ou de son règlement. La dateValidation est antérieure ou égale à la date d'approbation du document d'urbanisme auquel appartient la ZONE
    symbole varchar(20) NULL, -- Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles
    geom geometry(POLYGON, 3945) NULL, -- Géométrie de l'objet
    lib_idzone varchar(10) NULL, -- identifiant de la zone inter-procédure (doit être unique dans une procédure)
    lib_hstore hstore NULL, -- Hstore contenant tous les champs "lib_"
    lib_id_version integer NOT NULL, -- version de rattachement (FK)
    CONSTRAINT zoneurba_pkey PRIMARY KEY (idzoneurba)
);
CREATE INDEX sidx_zoneurba_geom ON urba_plui_ecriture.zoneurba USING gist (geom);
CREATE INDEX zoneurba_iddocumenturba_idx ON urba_plui_ecriture.zoneurba USING btree (idurba);
CREATE INDEX zoneurba_idurba_btree ON urba_plui_ecriture.zoneurba USING btree (idurba);
CREATE INDEX zoneurba_libelle_btree ON urba_plui_ecriture.zoneurba USING btree (libelle);
CREATE INDEX zoneurba_typezone_btree ON urba_plui_ecriture.zoneurba USING btree (typezone);

COMMENT ON TABLE urba_plui_ecriture.zoneurba IS 'Le code de l''urbanisme définit 4 zones : les zones (U), les zones (AU), les zones (A), les zones (N). A chaque zone est attaché un règlement. ';

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.zoneurba.idzoneurba IS 'Identifiant unique de la zone';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.idurba IS 'Identifiant du document d''urbanisme (détourné dans le plugin pour identifier la version)';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.libelle IS 'Nom court de la zone tel qu''il apparaît sur le plan de zonage';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.libelong IS 'Nom complet littéral de la zone tel qu''il apparaît dans le chapitre du règlement écrit';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.typezone IS 'Type de la zone classé dans une nomenclature simplifiée';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.formdomi IS 'Forme d''aménagement dominante souhaitée pour la zone afin de répondre aux besoins de réhabilitation, restructuration ou d''aménagement. Par exemple, une zone de type U peut voir sa forme urbaine dominante différer suivant qu''elle accueille préférentiellement tel type d''habitat ou d''équipement';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.nomfic IS 'Nom du fichier contenant le texte du règlement de la zone';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.destoui IS 'Destinations et sous-destinations autorisées';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.destcdt IS 'Destinations et sous-destinations conditionnées';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.destnon IS 'Destinations et sous-destinations non autorisées';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.urlfic IS 'Lien d''accès au fichier contenant le texte du règlement de la zone ou à defaut du règlement intégral indexé';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.datevalid IS 'Date de la dernière validation de la zone ou de son règlement. La dateValidation est antérieure ou égale à la date d''approbation du document d''urbanisme auquel appartient la zone';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.symbole IS 'Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.geom IS 'Géométrie de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.lib_idzone IS 'identifiant de la zone inter-procédure (doit être unique dans une procédure)';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.lib_hstore IS 'Hstore contenant tous les champs "lib_"';
COMMENT ON COLUMN urba_plui_ecriture.zoneurba.lib_id_version IS 'Version de rattachement (FK)';

-- DROP TABLE urba_plui_ecriture.zoneurbatype;

CREATE TABLE urba_plui_ecriture.zoneurbatype (
    id serial NOT NULL,
    typezone TEXT NULL,
    lib_typezone TEXT NULL,
    CONSTRAINT zoneurbatype_pkey PRIMARY KEY (id),
    CONSTRAINT zoneurbatype_unique UNIQUE (typezone)
);

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.zoneurbatype.id IS 'Identifiant unique';
COMMENT ON COLUMN urba_plui_ecriture.zoneurbatype.typezone IS 'Type de la zone classé dans une nomenclature simplifiée';
COMMENT ON COLUMN urba_plui_ecriture.zoneurbatype.lib_typezone IS 'Libellé du champ "typezone"';

-- FKEY
 ALTER TABLE urba_plui_ecriture.zoneurba ADD CONSTRAINT zoneurba_fk FOREIGN KEY (typezone) REFERENCES urba_plui_ecriture.zoneurbatype(typezone) ON DELETE RESTRICT ON UPDATE CASCADE;

-- DROP TABLE urba_plui_ecriture.formdomitype;

CREATE TABLE urba_plui_ecriture.formdomitype (
    id serial NOT NULL,
    formdomi char(4) NULL,
    lib_formdomi TEXT NULL,
    definition TEXT NULL,
    CONSTRAINT formdomitype_pkey PRIMARY KEY (id),
    CONSTRAINT formdomitype_unique UNIQUE (formdomi)
);

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.formdomitype.id IS 'Identifiant unique';
COMMENT ON COLUMN urba_plui_ecriture.formdomitype.formdomi IS 'Forme d''aménagement dominante souhaitée pour la zone';
COMMENT ON COLUMN urba_plui_ecriture.formdomitype.lib_formdomi IS 'Libellé du champ "formdomi"';
COMMENT ON COLUMN urba_plui_ecriture.formdomitype.definition IS 'Définition associée';

-- FKEY
 ALTER TABLE urba_plui_ecriture.zoneurba ADD CONSTRAINT formdomi_fk FOREIGN KEY (formdomi) REFERENCES urba_plui_ecriture.formdomitype(formdomi) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE urba_plui_ecriture.destinationtype (
    id serial NOT NULL,
    destination char(2) NULL,
    lib_destination TEXT NULL,
    CONSTRAINT destinationtype_pkey PRIMARY KEY (id),
    CONSTRAINT destinationtype_unique UNIQUE (destination)
);

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.destinationtype.id IS 'Identifiant unique';
COMMENT ON COLUMN urba_plui_ecriture.destinationtype.destination IS 'Destination et sous-destination d''une zone d''urbanisme';
COMMENT ON COLUMN urba_plui_ecriture.destinationtype.lib_destination IS 'Libellé du champ "destination"';

-- FKEY
-- FIXME : pas de jointure direte possible
-- ALTER TABLE urba_plui_ecriture.zoneurba ADD CONSTRAINT destination_fk FOREIGN KEY (destination) REFERENCES urba_plui_ecriture.destinationtype(destination) ON DELETE RESTRICT ON UPDATE CASCADE;

-- prescription #############################################################################################################################

-- DROP TABLE urba_plui_ecriture.prescription;

CREATE TABLE urba_plui_ecriture.prescription (
    idprescription serial NOT NULL, -- Identifiant unique de la prescription¶
    idurba varchar(30) NULL, -- Identifiant du document d'urbanisme (détourné dans le plugin pour identifier la version)
    libelle varchar(254) NULL, -- Libellé de la prescription
    txt varchar(10) NULL, -- Nom court de la prescription
    typepsc varchar(2) NULL, -- Type de la prescription
    stypepsc varchar(2) NULL, --Sous type de la prescription
    lib_typepsc_ct TEXT NULL, -- Type de prescription spécifique à la collectivité territoriale
    nature varchar(50) NULL, -- Libellé caractérisant un ensemble de prescriptions de même TYPEPSC-STYPEPSC
    nomfic varchar(80) NULL, -- Nom du fichier contenant le texte du règlement de la prescription
    urlfic varchar(254) NULL, -- Lien d''accès au fichier contenant le texte du règlement de la prescription ou à defaut du règlement intégral indexé
    datevalid date NULL, -- Date de la dernière validation de la prescription ou de son règlement. La dateValidation est antérieure ou égale à la date d''approbation du document d''urbanisme à laquelle appartient la prescription
    symbole varchar(20) NULL, -- Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles
    geom_pct geometry(POINT, 3945) NULL, -- Géométrie ponctuel de l'objet
    geom_lin geometry(LINESTRING, 3945) NULL, -- Géométrie linéaire de l'objet
    geom_surf geometry(POLYGON, 3945) NULL, -- Géométrie surfacique de l'objet
    lib_idpsc varchar(10) NULL, -- identifiant de la prescription inter-procédure (doit être unique dans une procédure)
    lib_hstore hstore NULL, -- Hstore contenant tous les champs "lib_"
    lib_id_version integer NOT NULL, -- version de rattachement (FK)
    CONSTRAINT prescription_pkey PRIMARY KEY (idprescription)
);
CREATE INDEX prescription_iddocumenturba_idx ON urba_plui_ecriture.prescription USING btree (idurba);
CREATE INDEX prescription_idurba_btree ON urba_plui_ecriture.prescription USING btree (idurba);
CREATE INDEX prescription_libelle_btree ON urba_plui_ecriture.prescription USING btree (libelle);
CREATE INDEX prescription_typepsc_btree ON urba_plui_ecriture.prescription USING btree (typepsc);
CREATE INDEX sidx_prescription_geom_lin ON urba_plui_ecriture.prescription USING gist (geom_lin);
CREATE INDEX sidx_prescription_geom_pct ON urba_plui_ecriture.prescription USING gist (geom_pct);
CREATE INDEX sidx_prescription_geom_surf ON urba_plui_ecriture.prescription USING gist (geom_surf);
COMMENT ON TABLE urba_plui_ecriture.prescription IS 'Table des prescriptions graphiques';

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.prescription.idprescription IS 'Identifiant unique de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.idurba IS 'Identifiant du document d''urbanisme (détourné dans le plugin pour identifier la version)';
COMMENT ON COLUMN urba_plui_ecriture.prescription.libelle IS 'Libellé de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.txt IS 'Nom court de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.typepsc IS 'Type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.stypepsc IS 'Sous type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.lib_typepsc_ct IS 'Type de prescription spécifique à la collectivité territoriale';
COMMENT ON COLUMN urba_plui_ecriture.prescription.nature IS 'Libellé caractérisant un ensemble de prescriptions de même TYPEPSC-STYPEPSC';
COMMENT ON COLUMN urba_plui_ecriture.prescription.nomfic IS 'Nom du fichier contenant le texte du règlement de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.urlfic IS 'Lien d''accès au fichier contenant le texte du règlement de la prescription ou à defaut du règlement intégral indexé';
COMMENT ON COLUMN urba_plui_ecriture.prescription.datevalid IS 'Date de la dernière validation de la prescription ou de son règlement. La dateValidation est antérieure ou égale à la date d''approbation du document d''urbanisme auquel appartient la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescription.symbole IS 'Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles';
COMMENT ON COLUMN urba_plui_ecriture.prescription.geom_pct IS 'Géométrie ponctuel de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.prescription.geom_lin IS 'Géométrie linéaire de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.prescription.geom_surf IS 'Géométrie surfacique de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.prescription.lib_idpsc IS 'identifiant de la prescription inter-procédure (doit être unique dans une procédure)';
COMMENT ON COLUMN urba_plui_ecriture.prescription.lib_hstore IS 'Hstore contenant tous les champs "lib_"';
COMMENT ON COLUMN urba_plui_ecriture.prescription.lib_id_version IS 'Version de rattachement (FK)';

CREATE TABLE urba_plui_ecriture.prescriptionurbatype (
    id serial NOT NULL, -- identifiant unique
    typepsc char(2) NULL, -- Type de la prescription
    lib_libelle_typepsc TEXT NULL, -- Libellé du type de la prescription
    stypepsc char(2) NULL, -- Sous type de la prescription
    lib_libelle_stypepsc TEXT NULL, -- Libellé du sous type de la prescription
    lib_ref_legis TEXT NULL, -- Article du code de l'urbanisme législatif
    lib_ref_regl TEXT NULL, -- Article du code de l'urbanisme règlementaire
    lib_supprime boolean NOT NULL DEFAULT FALSE, -- Indique si l''outil est encore mobilisable ou non
    lib_version_cnig_fin TEXT NULL, -- Version du standard CNIG sous laquelle le sous-code disparaît
    lib_remarques TEXT NULL,
    lib_cacher boolean NOT NULL DEFAULT FALSE,
    CONSTRAINT prescriptionurbatype_pkey PRIMARY KEY (id),
    CONSTRAINT prescriptionurbatype_unique UNIQUE (typepsc, stypepsc)
);
COMMENT ON TABLE urba_plui_ecriture.prescriptionurbatype IS 'Table de référence des différents types de prescriptions';

-- Column comments
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.id IS 'identifiant unique';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.typepsc IS 'Type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_libelle_typepsc IS 'Libellé du type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.stypepsc IS 'Sous-type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_libelle_stypepsc IS 'Libellé du sous type de la prescription';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_ref_legis IS 'Références législatives du code de l’urbanisme';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_ref_regl IS 'Références réglementaires du code de l’urbanisme ';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_supprime IS 'Indique si l''outil est encore mobilisable ou non (rend le sous-code non accessible pour la création de thématique)';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_version_cnig_fin IS 'Version du standard CNIG sous laquelle le sous-code disparaît';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_remarques IS 'Informations supplémentaires sur la raison de la disparition du sous-code';
COMMENT ON COLUMN urba_plui_ecriture.prescriptionurbatype.lib_cacher IS 'Permet de cacher le sous-code dans l''interface, par exemple s''il ne présente pas d''enjeu pour la collectivité';

-- FKEY
ALTER TABLE urba_plui_ecriture.prescription ADD CONSTRAINT prescription_typepsc_fk FOREIGN KEY (typepsc,stypepsc) REFERENCES urba_plui_ecriture.prescriptionurbatype(typepsc,stypepsc) ON DELETE RESTRICT ON UPDATE CASCADE;

-- information #############################################################################################################################

-- DROP TABLE urba_plui_ecriture.information;

CREATE TABLE urba_plui_ecriture.information (
    idinformation serial NOT NULL, -- Identifiant unique de l'information
    idurba varchar(30) NULL, -- Identifiant du document d'urbanisme (détourné dans le plugin pour identifier la version)
    libelle varchar(254) NULL, -- Libellé de la information
    txt varchar(10) NULL, -- Nom court de la information
    typeinf varchar(2) NULL, -- Type de la information
    stypeinf varchar(5) NULL, --Sous type de la information
    lib_typeinf_ct TEXT NULL, -- Type d'information spécifique à la collectivité territoriale
    nomfic varchar(80) NULL, -- Nom du fichier contenant le texte du règlement de la information
    urlfic varchar(254) NULL, -- Lien d''accès au fichier contenant le texte du règlement de la information ou à defaut du règlement intégral indexé
    datevalid date NULL, -- Date de la dernière validation de la information ou de son règlement. La dateValidation est antérieure ou égale à la date d''approbation du document d''urbanisme auquel appartient la information
    symbole varchar(20) NULL, -- Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles
    geom_pct geometry(POINT, 3945) NULL, -- Géométrie ponctuel de l'objet
    geom_lin geometry(LINESTRING, 3945) NULL, -- Géométrie linéaire de l'objet
    geom_surf geometry(POLYGON, 3945) NULL, -- Géométrie surfacique de l'objet
    lib_idinf varchar(10) NULL, -- identifiant de la information inter-procédure (doit être unique dans une procédure)
    lib_hstore hstore NULL, -- Hstore contenant tous les champs "lib_"
    lib_id_version integer NOT NULL, -- version de rattachement (FK)
    CONSTRAINT information_pkey PRIMARY KEY (idinformation)
);
CREATE INDEX information_iddocumenturba_idx ON urba_plui_ecriture.information USING btree (idurba);
CREATE INDEX information_idurba_btree ON urba_plui_ecriture.information USING btree (idurba);
CREATE INDEX information_libelle_btree ON urba_plui_ecriture.information USING btree (libelle);
CREATE INDEX information_typeinf_btree ON urba_plui_ecriture.information USING btree (typeinf);
CREATE INDEX sidx_information_geom_lin ON urba_plui_ecriture.information USING gist (geom_lin);
CREATE INDEX sidx_information_geom_pct ON urba_plui_ecriture.information USING gist (geom_pct);
CREATE INDEX sidx_information_geom_surf ON urba_plui_ecriture.information USING gist (geom_surf);
COMMENT ON TABLE urba_plui_ecriture.information IS 'Table des annexes';

-- Column comments
COMMENT ON COLUMN urba_plui_ecriture.information.idinformation IS 'Identifiant unique de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.idurba IS 'Identifiant du document d''urbanisme (détourné dans le plugin pour identifier la version)';
COMMENT ON COLUMN urba_plui_ecriture.information.libelle IS 'Libellé de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.txt IS 'Nom court de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.typeinf IS 'Type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.stypeinf IS 'Sous type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.lib_typeinf_ct IS 'Type d''information spécifique à la collectivité territoriale';
COMMENT ON COLUMN urba_plui_ecriture.information.nomfic IS 'Nom du fichier contenant le texte du règlement de l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.urlfic IS 'Lien d''accès au fichier contenant le texte du règlement de l''information ou à defaut du règlement intégral indexé';
COMMENT ON COLUMN urba_plui_ecriture.information.datevalid IS 'Date de la dernière validation de l''information ou de son règlement. La dateValidation est antérieure ou égale à la date d''approbation du document d''urbanisme auquel appartient l''information';
COMMENT ON COLUMN urba_plui_ecriture.information.symbole IS 'Symbolisation alternative, le cas échéant. Elle fait référence au registre des symboles';
COMMENT ON COLUMN urba_plui_ecriture.information.geom_pct IS 'Géométrie ponctuel de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.information.geom_lin IS 'Géométrie linéaire de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.information.geom_surf IS 'Géométrie surfacique de l''objet';
COMMENT ON COLUMN urba_plui_ecriture.information.lib_idinf IS 'identifiant de l''information inter-procédure (doit être unique dans une procédure)';
COMMENT ON COLUMN urba_plui_ecriture.information.lib_hstore IS 'Hstore contenant tous les champs "lib_"';
COMMENT ON COLUMN urba_plui_ecriture.information.lib_id_version IS 'Version de rattachement (FK)';

CREATE TABLE urba_plui_ecriture.informationurbatype (
    id serial NOT NULL, -- identifiant unique
    typeinf char(2) NULL, -- Type de l'information
    lib_libelle_typeinf TEXT NULL, -- Libellé du type de l''information
    stypeinf char(2) NULL, -- Sous type de l'information
    lib_libelle_stypeinf TEXT NULL, -- Libellé du sous type de l'information
    lib_ref_legis TEXT NULL,
    lib_ref_regl TEXT NULL, -- Références pour l'annexion
    lib_supprime boolean NOT NULL DEFAULT FALSE, -- Indique si l''outil est encore mobilisable ou non
    lib_version_cnig_fin TEXT NULL, -- Version du standard CNIG sous laquelle le sous-code disparaît
    lib_remarques TEXT NULL,
    lib_cacher boolean NOT NULL DEFAULT FALSE,
    CONSTRAINT informationurbatype_pkey PRIMARY KEY (id),
    CONSTRAINT informationurbatype_unique UNIQUE (typeinf,stypeinf)
);
COMMENT ON TABLE urba_plui_ecriture.informationurbatype IS 'Table de référence des différents types d''informations';

-- Column comments
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.id IS 'identifiant unique';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.typeinf IS 'Type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_libelle_typeinf IS 'Libellé du type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.stypeinf IS 'Sous type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_libelle_stypeinf IS 'Libellé du sous type de l''information';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_ref_legis IS 'Références pour la définition de l’information';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_ref_regl IS 'Références pour l''annexion';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_supprime IS 'Indique si l''outil est encore mobilisable ou non (rend le sous-code non accessible pour la création de thématique)';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_version_cnig_fin IS 'Version du standard CNIG sous laquelle le sous-code disparaît';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_remarques IS 'Informations supplémentaires sur la raison de la disparition du sous-code';
COMMENT ON COLUMN urba_plui_ecriture.informationurbatype.lib_cacher IS 'Permet de cacher le sous-code dans l''interface, par exemple s''il ne présente pas d''enjeu pour la collectivité';

-- FKEY
ALTER TABLE urba_plui_ecriture.information ADD CONSTRAINT information_fk FOREIGN KEY (typeinf,stypeinf) REFERENCES urba_plui_ecriture.informationurbatype(typeinf,stypeinf) ON DELETE RESTRICT ON UPDATE CASCADE;



-- habillage #################################################################################################################################################
-- table non utilisée dans le plugin
-- elle pourrait servir à l'habillage des atlas/plan
-- l'idée serait d'avoir un tuple par atlas/plan et par procédure

-- DROP TABLE urba_plui_ecriture.habillage;

CREATE TABLE urba_plui_ecriture.habillage (
    idhabillage serial NOT NULL,
    idurba varchar(30) NULL,
    nattrac varchar(40) NULL,
    txt varchar(80) NULL,
    police varchar(40) NULL,
    taille int4 NULL,
    "style" varchar(40) NULL,
    angle int4 NULL,
    geom_txt geometry(POINT, 3945) NULL,
    geom_pct geometry(POINT, 3945) NULL,
    geom_lin geometry(LINESTRING, 3945) NULL,
    geom_surf geometry(POLYGON, 3945) NULL,
    lib_hstore hstore NULL, -- Hstore contenant tous les champs "lib_"
    couleur varchar(11) NULL,
    natecr varchar(40) NULL,
    lib_id_version integer NOT NULL, -- version de rattachement (FK)
    CONSTRAINT habillage_pkey PRIMARY KEY (idhabillage)
);
CREATE INDEX habillage_iddocumenturba_idx ON urba_plui_ecriture.habillage USING btree (idurba);
CREATE INDEX sidx_habillage_geom_lin ON urba_plui_ecriture.habillage USING gist (geom_lin);
CREATE INDEX sidx_habillage_geom_pct ON urba_plui_ecriture.habillage USING gist (geom_pct);
CREATE INDEX sidx_habillage_geom_surf ON urba_plui_ecriture.habillage USING gist (geom_surf);
CREATE INDEX sidx_habillage_geom_txt ON urba_plui_ecriture.habillage USING gist (geom_txt);
COMMENT ON TABLE urba_plui_ecriture.habillage IS 'Table d''habillage des documents graphiques';

-- Column comments

COMMENT ON COLUMN urba_plui_ecriture.habillage.lib_hstore IS 'Hstore contenant tous les champs "lib_" (non critiques au MCD)';
