-- PLUI versionning


-- ##################################################################################################################################################
-- Gestion des thématiques
-- ##################################################################################################################################################

-- Utile ?
CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_pluiv_thematiques()
 RETURNS trigger
 LANGUAGE plpgsql
 STABLE
AS $function$
BEGIN

    IF TG_OP = 'INSERT' THEN
--
--        perform urba_plui_ecriture.qgis_creer_thematique(NEW);

--    ELSIF TG_OP = 'UPDATE' AND NOT NEW.obsolete THEN -- Mise à jour de la thématique

--        perform urba_plui_ecriture.func_verifier_modification_thematique(NEW.classe_entite, OLD.categories, NEW.categories);

--    ELSIF TG_OP = 'UPDATE' AND NEW.obsolete THEN -- Désactivation de la thématique

--        perform urba_plui_ecriture.func_verifier_desactivation_thematique(NEW.id);

    ELSIF TG_OP = 'DELETE' THEN

        perform urba_plui_ecriture.func_verifier_suppression_thematique(NEW.id);

    END IF;

    RETURN NEW;
END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.tf_pluiv_thematiques() IS 'Fonction trigger de gestion des thématiques';

-- trigger sur table
-- Table Triggers

--CREATE TRIGGER tt_pluiv_thematiques_delete BEFORE
--DELETE
--    OR
--UPDATE
--    ON
--    urba_plui_ecriture.pluiv_thematiques FOR EACH ROW WHEN ((pg_trigger_depth() = 0)) EXECUTE FUNCTION urba_plui_ecriture.tf_pluiv_thematiques();

--CREATE TRIGGER tt_pluiv_thematiques_insert AFTER
--INSERT
--    ON
--    urba_plui_ecriture.pluiv_thematiques FOR EACH ROW EXECUTE FUNCTION urba_plui_ecriture.tf_pluiv_thematiques();
--ALTER TABLE urba_plui_ecriture.pluiv_thematiques DISABLE TRIGGER tt_pluiv_thematiques_insert;


-- ##################################################################################################################################################
-- Gestion des champs lib_
-- ##################################################################################################################################################

CREATE OR REPLACE FUNCTION urba_plui_ecriture.tf_pluiv_champs_lib()
 RETURNS trigger
 LANGUAGE plpgsql
 STABLE
AS $function$
BEGIN

    IF TG_OP = 'INSERT' THEN
        CASE
            WHEN NEW.nom_col NOT LIKE 'lib\_%'
                THEN RAISE EXCEPTION 'Le champ doit commencer par "lib_" !';
            WHEN NEW.classe_entite NOT IN ('zoneurba', 'prescription', 'information', 'habillage')
                THEN RAISE EXCEPTION 'Le champ "classe_entite" doit faire partie des valeurs suivantes : ''zoneurba'', ''prescription'', ''information'' ou ''habillage''';
            ELSE
                PERFORM urba_plui_ecriture.admin_supprimer_vues_entite(NEW.classe_entite);
            END CASE;

        -- UPDATE possible seulement dans le cas où le nom du champ ne change pas
    ELSIF TG_OP = 'UPDATE'
        AND NEW.classe_entite = OLD.classe_entite
        AND NEW.nom_col = OLD.nom_col
    THEN
        --PERFORM urba_plui_ecriture.admin_supprimer_vues_entite(NEW.classe_entite);

    ELSIF TG_OP = 'UPDATE' THEN
        RAISE EXCEPTION 'Pas de modification possible !';
        -- MAJ du champ dans tous les objets de la même classe d'entité
        -- (h - from_key) || hstore(to_key, h -> from_key)
        -- A intégrer dans fonction prenant en compte la classe d'entité et les objets NEW et OLD en hstore

        -- Redéploiement des vues
        --perform urba_plui_ecriture.admin_deployer_vues_entite(NEW.classe_entite);


    ELSIF TG_OP = 'DELETE' THEN
        RAISE EXCEPTION 'Pas de suppression possible !';
        --perform urba_plui_ecriture.supprimer_zonage(hstore(OLD));

    END IF;

    RETURN NEW;
END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.tf_pluiv_champs_lib() IS 'Fonction trigger de redéploiement des vues des classes d''entités';

-- trigger sur table
-- Table Triggers

CREATE TRIGGER t_pluiv_champs_lib AFTER
INSERT
    OR
DELETE
    OR
UPDATE
    ON
    urba_plui_ecriture.pluiv_champs_lib FOR EACH ROW EXECUTE FUNCTION urba_plui_ecriture.tf_pluiv_champs_lib();

-- plus utilisé : alerte sur métagénération de vues automatique (il semble préférable de le faire à la main)
ALTER TABLE urba_plui_ecriture.pluiv_champs_lib DISABLE TRIGGER t_pluiv_champs_lib;
