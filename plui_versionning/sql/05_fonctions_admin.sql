-- PLUI versionning

-- ANCIENNE FONCTION pour le reviewcode ... A SUPPRIMER AVANT LE MERGE

-- urba_plui_ecriture.admin_approbation_plu
-- urba_plui_ecriture.admin_copier_donnees_entre_schemas
-- urba_plui_ecriture.admin_init_globale_bdd
-- urba_plui_ecriture.admin_initier_nouveau_cycle (cycle = procédure dans la nouvelle version)


-- ##########################################################################################################################################
-- urba_plui_ecriture.admin_deployer_vues_entite
-- ##########################################################################################################################################

-- Fonction qui permet de créer des vues de travail en déployant les données contenues dans le lib_hstore de façon "explicite"
-- ATTENTION : cette fonction est fixée sur le CNIG 2017

CREATE OR REPLACE FUNCTION urba_plui_ecriture.admin_deployer_vues_entite(classe character)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE

    type_classe_entite     record;
    champs_fixes character varying;
    champs_lib   character varying;
    query        text;

BEGIN
    RAISE NOTICE 'classe_entite : %', classe;


    -- Récupération des différentes géométries par classe d'entité
    FOR type_classe_entite IN
                                SELECT
                                    prefixe,
                                    nom_colonne_geom,
                                    COALESCE(right(nom_colonne_geom, -4)) AS suffixe,
                                    CASE
                                        WHEN right(nom_colonne_geom, -4) IN ('_pct','_txt') THEN 'Point'
                                        WHEN right(nom_colonne_geom, -4) = '_lin' THEN 'Linestring'
                                        WHEN right(nom_colonne_geom, -4) = '_surf' THEN 'Polygon'
                                        ELSE 'Polygon'
                                    END AS geomtype
                                FROM urba_plui_ecriture.pluiv_structure_entites struct
                                WHERE classe_entite = $1
                                  AND cnig = '2017' -- ATTENTION : cette fonction est fixée sur le CNIG 2017
        LOOP

            raise notice 'type_classe_entite : %', type_classe_entite;

            -- Récupération des colonnes fixes

            SELECT string_agg(column_name, ',')
            INTO champs_fixes
            FROM information_schema.columns
            WHERE table_schema = 'urba_plui_ecriture'
              AND table_name = '' || $1 || ''
              AND column_name <> 'lib_hstore' -- exclusion du hstore
              AND column_name NOT LIKE 'geom%' -- exclusion des champs de géométrie
            ;


            -- TODO : rustine pour prendre en compte l'exception des couches d'habillage. Voir pour solution pérenne
            IF classe = 'habillage' AND type_classe_entite.nom_colonne_geom = 'geom_txt'
                THEN champs_fixes = 'idhabillage,natecr,txt,police,taille,style,couleur,angle,idurba,lib_id_version';
            ELSIF classe = 'habillage'
                THEN champs_fixes = 'idhabillage,nattrac,couleur,idurba,lib_id_version';
            END IF;

            -- Récupération des colonnes "lib_"
            EXECUTE 'WITH query AS (
                SELECT nom_col
                FROM  urba_plui_ecriture.pluiv_champs_lib
                WHERE classe_entite = ' || quote_literal(classe) || '
                AND deployable
                ORDER BY 1
                ) SELECT string_agg(quote_literal(nom_col)  || '' AS '' || quote_ident(nom_col), '', h->'') FROM query' INTO champs_lib;

            RAISE NOTICE 'champs_fixes : %', champs_fixes;
            RAISE NOTICE 'champs_lib : %', champs_lib;
            RAISE NOTICE 'champ_geom : %', type_classe_entite.nom_colonne_geom;
            RAISE NOTICE 'suffixe : %', type_classe_entite.suffixe;


            -- Redéploiement des vues des entités existantes
            query = 'DROP VIEW IF EXISTS urba_plui_ecriture.v_' || classe || type_classe_entite.suffixe || ' CASCADE;
            CREATE OR REPLACE VIEW urba_plui_ecriture.v_' || classe || type_classe_entite.suffixe || ' AS
                SELECT ' || champs_fixes || ', ' ||
                    'h-> ' || champs_lib || ', ' ||
                    type_classe_entite.nom_colonne_geom || '::geometry(' || type_classe_entite.geomtype || ', 3945) AS ' || type_classe_entite.nom_colonne_geom || '
                FROM  (SELECT ' || champs_fixes || ', lib_hstore AS h,' ||
                                    type_classe_entite.nom_colonne_geom  ||
                        ' FROM urba_plui_ecriture.' || classe || ' classe
                        JOIN urba_plui_ecriture.pluiv_versions v ON v.id = classe.lib_id_version
                        WHERE ' || type_classe_entite.nom_colonne_geom || ' IS NOT NULL ' ||
                        'AND lib_hstore -> ''lib_modif'' != ''S'') t
                JOIN urba_plui_ecriture.pluiv_versions v ON v.id = t.lib_id_version
                JOIN urba_plui_ecriture.pluiv_projets p ON p.id = v.id_projet ;

                CREATE TRIGGER tv_' || classe || '
              INSTEAD OF INSERT OR UPDATE OR DELETE
              ON urba_plui_ecriture.v_' || classe || type_classe_entite.suffixe || '
              FOR EACH ROW
              EXECUTE PROCEDURE urba_plui_ecriture.tf_' || classe || '(''' || type_classe_entite.prefixe || ''');';

--            RAISE NOTICE 'query : %', query;
            EXECUTE query;

            -- Redéploiement des vues des entités supprimées
            query = 'DROP VIEW IF EXISTS urba_plui_ecriture.v_' || classe || type_classe_entite.suffixe || '_suppr CASCADE;
            CREATE OR REPLACE VIEW urba_plui_ecriture.v_' || classe || type_classe_entite.suffixe || '_suppr AS
                SELECT ' || champs_fixes || ', ' ||
                    'h-> ' || champs_lib || ', ' ||
                    type_classe_entite.nom_colonne_geom || '::geometry(' || type_classe_entite.geomtype || ', 3945) AS ' || type_classe_entite.nom_colonne_geom || '
                FROM  (SELECT ' || champs_fixes || ', lib_hstore AS h,' ||
                                    type_classe_entite.nom_colonne_geom  ||
                        ' FROM urba_plui_ecriture.' || classe || ' classe
                        JOIN urba_plui_ecriture.pluiv_versions v ON v.id = classe.lib_id_version
                        WHERE ' || type_classe_entite.nom_colonne_geom || ' IS NOT NULL ' ||
                        'AND lib_hstore -> ''lib_modif'' = ''S'') t
                JOIN urba_plui_ecriture.pluiv_versions v ON v.id = t.lib_id_version
                JOIN urba_plui_ecriture.pluiv_projets p ON p.id = v.id_projet;';

--            RAISE NOTICE 'query 2 : %', query;
            EXECUTE query;

        END LOOP;


END;
$function$
;

COMMENT ON FUNCTION urba_plui_ecriture.admin_deployer_vues_entite(bpchar) IS 'Fonction de redéploiement des vues de la classe d''entité renseignée (vues des entités courantes, des entités supprimées)';
