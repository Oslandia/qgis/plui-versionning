<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.6.1-Brighton" minimumScale="-4.65661e-10" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <edittypes>
    <edittype widgetv2type="TextEdit" name="IDURBA">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="ValueMap" name="TYPEDOC">
      <widgetv2config fieldEditable="1" labelOnTop="0">
        <value key="CC-Carte communale" value="CC"/>
        <value key="PLU-Plan local d'urbanisme" value="PLU"/>
        <value key="POS-Plan d'occupation des sols" value="POS"/>
        <value key="PSMV-Plan de sauvegarde et de mise en valeur" value="PSMV"/>
      </widgetv2config>
    </edittype>
    <edittype widgetv2type="TextEdit" name="DATAPPRO">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="DATEFIN">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="INTERCO">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="SIREN">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="ValueMap" name="ETAT">
      <widgetv2config fieldEditable="1" labelOnTop="0">
        <value key="01-En cours de procédure" value="01"/>
        <value key="02-Arrêté" value="02"/>
        <value key="03-Opposable" value="03"/>
        <value key="04-Annulé" value="04"/>
        <value key="05-Remplacé" value="05"/>
        <value key="06-Abrogé" value="06"/>
        <value key="07-Approuvé" value="07"/>
      </widgetv2config>
    </edittype>
    <edittype widgetv2type="TextEdit" name="NOMREG">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="URLREG">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="NOMPLAN">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="URLPLAN">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="TextEdit" name="SITEWEB">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
    <edittype widgetv2type="ValueMap" name="TYPEREF">
      <widgetv2config fieldEditable="1" labelOnTop="0">
        <value key="01-PCI" value="01"/>
        <value key="02-BD Parcellaire" value="02"/>
      </widgetv2config>
    </edittype>
    <edittype widgetv2type="TextEdit" name="DATEREF">
      <widgetv2config IsMultiline="0" fieldEditable="1" UseHtml="0" labelOnTop="0"/>
    </edittype>
  </edittypes>
  <editform></editform>
  <editforminit></editforminit>
  <featformsuppress>0</featformsuppress>
  <annotationform></annotationform>
  <editorlayout>generatedlayout</editorlayout>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <attributeactions/>
</qgis>
