# Procédure

pour construire les tables de références des catégories de prescriptions/informations, voici les étapes :

1. Récupérer dernière documentation CNIG (v2022-10) ici : http://cnig.gouv.fr/IMG/pdf/230112_standard_cnig_plu_v2022-10.pdf
2. Importer tableaux de prescriptions/informations dans le tableau Excel "cnig_plu_codes.xlsx" via l'outil d'import depuis une source PDF
3. Créer respectivement une feuille Prescription et Information
4. Reporter les imports dans ces feuilles
5. Redresser l'historique des données avec les versions CNIG précédentes (http://cnig.gouv.fr/historique-ddu-a21369.html)
6. Importer les données en base avec le workbench FME "import_cnig_plu_codes.fmw" (penser à créer la base en amont)
