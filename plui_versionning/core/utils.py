import re
from enum import Enum

from qgis.PyQt.QtGui import QColor


class ClasseEntite:
    ZONAGE = {"id": 0, "name": "Zonage", "thematique": "ZON", "code": "zon"}
    PRESCRIPTION = {"id": 1, "name": "Prescription", "thematique": "P", "code": "psc"}
    INFORMATION = {"id": 2, "name": "Information", "thematique": "I", "code": "inf"}
    HABILLAGE = {"id": 3, "name": "Habillage", "thematique": "H", "code": "hab"}


class TreeObject(Enum):
    THEMATIQUE = 0
    PROJET = 1
    VERSION = 2


CLASSES_ENTITE = [
    ClasseEntite.ZONAGE,
    ClasseEntite.PRESCRIPTION,
    ClasseEntite.INFORMATION,
    ClasseEntite.HABILLAGE,
]

COLORS = {
    "Référence": QColor(50, 150, 235),
    "En cours": QColor(235, 180, 50),
    "Figée": QColor(235, 65, 50),
    "Abandonnée": QColor(55, 55, 55),
    "Validée": QColor(20, 125, 10),
}


def catch_postgres_exception(msg: str) -> str:
    """Catch message sent by PostgreSQL function by using `raise exception`

    Args:
        msg (str): Complete exception message

    Returns:
        str: Message sent by PostgreSQL when using `raise exception`
    """
    m = re.search("ERR(.*): (.*)", msg)
    if m:
        return m.group(2)
    return msg
