from pathlib import Path

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTabWidget

from plui_versionning.core.utils import CLASSES_ENTITE
from plui_versionning.gui.wdg_thematique import ThematiqueWidget
from plui_versionning.toolbelt import PlgLogger, PlgOptionsManager


class ThematiqueTabWidget(QTabWidget):
    def __init__(self, parent: QTabWidget = None):
        """
        QTabWidget Thématiques

        Args:
            parent: parent QTabWidget
        """
        super().__init__(parent)
        self.parent = parent
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager()

        # DB connection
        self.db_connection_uri = self.plg_settings.get_plg_settings().db_connection_uri
        self.db_schema_name = self.plg_settings.get_plg_settings().db_schema_name

        uic.loadUi(Path(__file__).parent / f"{Path(__file__).stem}.ui", self)

        for classe in CLASSES_ENTITE:
            self.addTab(ThematiqueWidget(classe), classe["name"])

    def set_procedure(self, id_procedure: int):
        for i in range(self.count()):
            self.widget(i).set_procedure(id_procedure)
