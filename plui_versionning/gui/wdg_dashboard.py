from pathlib import Path

from qgis.core import QgsApplication
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu, QWidget

# project
from plui_versionning.gui.dlg_manage_styles import ManageStylesDialog
from plui_versionning.gui.dlg_manage_thematiques import ManageThematiquesDialog
from plui_versionning.toolbelt import PlgLogger, PlgOptionsManager


class PLUiDashboard(QWidget):
    """PLUi Dashboard"""

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager()

        # DB connection
        self.db_connection_uri = self.plg_settings.get_plg_settings().db_connection_uri
        self.db_schema_name = self.plg_settings.get_plg_settings().db_schema_name

        uic.loadUi(Path(__file__).parent / f"{Path(__file__).stem}.ui", self)

        menu_icon = QIcon(QgsApplication.iconPath("propertyicons/settings.svg"))
        self.btn_dashboard_menu.setIcon(menu_icon)
        self.action_manage_styles = QAction(
            icon=QIcon(QgsApplication.iconPath("styleicons/paletted.svg")),
            text=self.tr("Gérer les styles"),
        )
        self.action_manage_styles.triggered.connect(self.manage_styles)
        self.action_manage_thematiques = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionLayoutManager.svg")),
            text=self.tr("Gérer les thématiques"),
        )
        self.action_manage_thematiques.triggered.connect(self.manage_thematiques)
        self.btn_dashboard_menu.setMenu(QMenu())
        self.btn_dashboard_menu.menu().addAction(self.action_manage_styles)
        self.btn_dashboard_menu.menu().addAction(self.action_manage_thematiques)

    def set_procedure(self, id_procedure: int):
        self.wdg_tab_thematique.set_procedure(id_procedure)

    def manage_styles(self):
        dlg_manage_styles = ManageStylesDialog(parent=self)
        dlg_manage_styles.show()

    def manage_thematiques(self):
        dlg_manage_thematiques = ManageThematiquesDialog(parent=self)
        dlg_manage_thematiques.show()

    # def fill_status_legend(self):
    #     """Set legend for version status with colored labels in groupbox"""
    #     # Create a layout in groupbox
    #     vlayout = QVBoxLayout()
    #     for key, color in colors.items():
    #         # Create one label for each color
    #         label = QLabel(key)
    #         label.setStyleSheet("background-color:" + color.name())
    #         vlayout.addWidget(label)
    #     self.groupbox_status.setLayout(vlayout)
