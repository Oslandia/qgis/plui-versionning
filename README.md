# PLU Versionning - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)


[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

## Résumé

Basé sur l'usage conjoint de [QGIS](https://qgis.org/fr/site/) et [PostgreSQL](https://www.postgresql.org/), cet outil, développé par [l’AURG](https://www.aurg.fr/) et [GAM](https://www.grenoblealpesmetropole.fr/), vise à faciliter la souplesse et la traçabilité dans la numérisation des documents d'urbanisme. Initialement basé sur un projet QGIS simple, cet outil a été repris par [Oslandia](https://oslandia.com/) pour être transformé en plugin (à l’image du plugin [cadastre](https://plugins.qgis.org/plugins/cadastre/)).

Ce plugin repose sur le standard [CNIG 2017 et 2024](https://cnig.gouv.fr/ressources-dematerialisation-documents-d-urbanisme-a2732.html) pour répondre aux exigences légales de dématérialisation des documents d'urbanisme (DDU), rendant son utilisation possible pour tous les territoires concernés par la numérisation d’un PLU(i).

L'outil propose une historisation des différentes versions de travail, détaillée dans **[le principe de versioning](https://gitlab.com/Oslandia/qgis/plui-versionning/-/wikis/home)**.

### Principe rapide

Chaque procédure inclut plusieurs thématiques : le zonage, les prescriptions (espaces boisés, limitations, emplacements réservés, etc.) et les informations (périmètres, ZAC, etc.). Vous pouvez gérer tous les codes et sous-codes conformes au standard CNIG.

Pour chaque thématique/procédure, il peut y avoir un ou plusieurs projets, chaque projet pouvant inclure plusieurs versions. Les versions possèdent des statuts (ex. "en cours", "figée", "abandonnée") permettant de suivre l'avancement des procédures. Elles peuvent être facilement dupliquées pour travailler sur différentes hypothèses.

En fin de procédure, une version par projet est validée, et les projets constituent le PLU(i).

Pour plus de détails, consultez le [wiki](https://gitlab.com/Oslandia/qgis/plui-versionning/-/wikis/home).

## Mise en place

Le répertoire `sql` contient les scripts SQL à exécuter dans une base PostgreSQL (avec l'extension PostGIS). Ils doivent être exécutés dans l'ordre de leur numérotation.

Le script 15 est facultatif : il s'agit d'un jeu de données provenant du PLUI de Grenoble Alpes Métropole.

⚠️ **Attention** : l'extension HSTORE doit être installée. Les colonnes HSTORE permettent de gérer les colonnes supplémentaires "lib_" (cf. standard CNIG) sans modification des MCD du plugin. La table `pluiv_champs_lib` gère les colonnes "lib_" pour chaque entité (zonage, prescription, information).

Les styles sont stockés en base. La table `pluiv_styles` fait le lien entre thématique/procédure et le style enregistré dans `public.layer_style`.  
TODO : compléter le tableau de bord "gérer les styles".

## Intégration des données au format CNIG

Les données intégrées doivent respecter le format CNIG (téléchargeable depuis le portail de l'urbanisme).

Les fichiers SHP et `doc_urba` doivent être chargés dans un schéma en base.  
TODO : développer une interface de chargement depuis des SHP ou directement depuis la base.

Le script `plui_versionning/sql/99_integration_donnees_cnig.sql` crée une fonction `admin_integration_cnig`.

### Notes d'utilisation de la fonction

```sql
SELECT urba_plui_ecriture.admin_integration_cnig(:code_insee_ct, :date_appro, :sch_source, :sch_dest, :vide_table, :data_hstore_zon, :data_hstore_pre);  
```

Où :

- `code_insee_ct` → Code INSEE de la collectivité territoriale
- `date_appro` → Date d'approbation au format AAAAMMJJ
- `sch_source` → Nom du schéma contenant les données d'origine
- `sch_dest` → Schéma où les scripts ont été exécutés (schéma du plugin)
- `vide_table` → Si "true", le script vide les tables (utile pour les tests d'intégration)
- `data_hstore_zon` → Champ au format JSON pour intégrer des données "LIB_" supplémentaires pour la couche zonage (cf. documentation CNIG)
- `data_hstore_pre` → Champ au format JSON pour intégrer des données "LIB_" supplémentaires pour la couche prescription (cf. documentation CNIG)

### Origine des données : vues ou tables au format CNIG dans le schéma `sch_source`

Conformément au standard CNIG :

#### Format de la table/vue `<CODE INSEE>_DOC_URBA_<DATE APPRO>`

| Nom de la colonne | Type de données |
|-------------------|-----------------|
| IDURBA           | varchar(30)     |
| TYPEDOC          | varchar(4)      |
| ETAT             | varchar(2)      |
| NOMPROC          | varchar(10)     |
| DATAPPRO         | varchar(8)      |
| DATEFIN          | varchar(8)      |
| SIREN            | bpchar(9)       |
| NOMREG           | varchar(80)     |
| URLREG           | varchar(254)    |
| NOMPLAN          | varchar(80)     |
| URLPLAN          | varchar(254)    |
| URLPE            | varchar(254)    |
| SITEWEB          | varchar(254)    |
| TYPEREF          | varchar(2)      |
| DATEREF          | varchar(8)      |

#### Format de la table/vue `<CODE INSEE>_ZONE_URBA_<DATE APPRO>`

> ⚠️ Les colonnes `LIB_ATTRx` et `LIB_VALx` sont propres à GAM. Si vous les utilisez, configurez le champ `data_hstore_zon` dans la fonction.

| Nom de la colonne | Type de données             |
|-------------------|-----------------------------|
| LIB_IDZONE        | text                        |
| LIBELLE           | varchar(12)                 |
| LIBELONG          | text                        |
| TYPEZONE          | varchar(3)                  |
| NOMFIC            | varchar(80)                 |
| URLFIC            | varchar(254)                |
| IDURBA            | varchar(30)                 |
| DATVALID          | date                        |
| LIB_ATTR1         | text                        |
| LIB_VAL1          | text                        |
| LIB_ATTR2         | text                        |
| LIB_VAL2          | text                        |
| geom              | geometry(polygon, 3945)     |

#### Format des tables prescription et information

Se référer au standard pour plus de détails.

##### Les libellés de la collectivité

Les libellés spécifiques de la collectivité doivent être listés dans les tables `pluiv_zoneurbatype_ct`, `pluiv_prescriptionurbatype_ct` et `pluiv_informationurbatype_ct`.

Vous pouvez prendre exemple sur les INSERT dans le script `99_tests.sql` (exemple pour la commune de Villard-Bonnot, code INSEE 38547).


## TODO  

- fonctionnalités
- captures d'écran

## Generated options

### Plugin

"plugin_name": PLU Versionning
"plugin_name_slug": plui_versionning
"plugin_name_class": PluiVersionning

"plugin_category": Database
"plugin_description_short": Outil de suivi des versions de numérisation des documents d'urbanisme au format CNIG.
"plugin_description_long": Outil de suivi des versions de numérisation des documents d'urbanisme au format CNIG.
"plugin_description_short": Outil de suivi des versions de numérisation des documents d'urbanisme au format CNIG.
"plugin_icon": default_icon.png

"author_name": Bruno DEFRANCE, Eric VINOUZE
"author_email": qgis@oslandia.com

"qgis_version_min": 3.16
"qgis_version_max": 3.99

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: Flake8

See also: [contribution guidelines](CONTRIBUTING.md).

## CI/CD

Plugin is linted, tested, packaged and published with GitLab.

If you mean to deploy it to the [official QGIS plugins repository](https://plugins.qgis.org/), remember to set your OSGeo credentials (`OSGEO_USER_NAME` and `OSGEO_USER_PASSWORD`) as environment variables in your CI/CD tool.


### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <https://gitlab.com/Oslandia/qgis/plui-versionning/-/wikis/home>
- repository: <https://gitlab.com/Oslandia/qgis/plui_versionning>
- tracker: <None>

----

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
