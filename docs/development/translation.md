# Manage translations

## Requirements

Qt Linguist tools are used to manage translations. Typically on Ubuntu:

```bash
sudo apt install qt5-qmake qttools5-dev-tools
```

## Workflow

1. Write text and messages in English
1. Make sure to list every Python file with translatable strings in `SOURCES` section of `plui_versionning/resources/i18n/plugin_translation.pro`
1. Make sure to list every Qt UI file with translatable strings in `FORMS` section of `plui_versionning/resources/i18n/plugin_translation.pro`
1. Update `.ts` files:

    ```bash
    pylupdate5 -noobsolete -verbose plui_versionning/resources/i18n/plugin_translation.pro
    ```

1. Translate your text using QLinguist or directly into `.ts` files.
1. Compile it:

    ```bash
    lrelease plui_versionning/resources/i18n/*.ts
    ```
