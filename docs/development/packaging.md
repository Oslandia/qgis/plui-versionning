# Packaging and deployment

## Packaging

This plugin is using the [qgis-plugin-ci](https://github.com/opengisch/qgis-plugin-ci/) tool to perform packaging operations.

```bash
python -m pip install -U -r requirements/packaging.txt
```

Then use it:

```bash
qgis-plugin-ci package 1.5.2
```

## Release a version

Through git workflow:

1. Fulfill the `CHANGELOG.md`
2. Apply a git tag with the relevant version: `git tag -a 0.3.0 {git commit hash} -m "New plugin UI"`
3. Push tag to master
