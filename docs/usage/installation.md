# Installation

## Version stable (recommandée)

Le plugin est publié sur le dépôt officiel des extensions de QGIS : <https://plugins.qgis.org/plugins/plui_versionning>.

## Versions expérimentales

Des versions intermédiaires (alpha, beta...) sont parfois publiées sur le dépôt officiel dans le canal expérimental.

Pour y accéder, il suffit d'activer les extensions expérimentales dans les préférences du gestionnaire d'extensions de QGIS.

## Version en développement

Si vous vous considérez comme un *early adopter*, un testeur ou que vous ne pouvez attendre qu'une version soit publiée (même dans le canal expérimental !), vous pouvez utiliser la version automatiquement packagée pour chaque commit poussé sur la branche principale.

Pour cela, il faut ajouter cette URL dans les dépôts référencés dans le gestionnaire d'extensions de QGIS :

```html
https://oslandia.gitlab.io/qgis/plui-versionning/plugins.xml
```

Dans QGIS :

1. Menu `Extensions` > `Installer/Gérer des extensions`
2. Onglet `Paramètres`

    ![QGIS - Plugin repositories](https://docs.qgis.org/3.34/fr/_images/plugins_settings.png)

3. Sous la liste des dépôts, cliquer sur `Ajouter...` et renseigner l'URL ci-dessus

    ![QGIS - Détail du dépôt de plugins](./static/qgis_plugin_installation_custom_repository.webp)

4. Une fois le dépôt ajouté, l'extension devrait apparaître dans l'onglet `Non installées`.

:::{warning}
Selon votre configuration, redémarrer QGIS peut être nécessaire, le gestionnaire d'extensions ayant des comportements parfois capricieux par rapport aux dépôts tiers.
:::
