# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.3.0-beta3 - 2024-11-04

- correction de la version de qgis-plugin-ci utilisée pour publier le plugin et tenir compte des dernières règles de publication de plugins.qgis.org

## 0.3.0-beta2 - 2024-11-04

- correction de la CI/CD
- ajout de la section oslandia aux métadonnées

## 0.3.0-beta1 - 2024-11-04

- première publication sur <https://plugins.qgis.org>

## 0.2.0 - 2023-07-26

- Release for all the new functionnalities

## 0.1.0 - 2021-11-29

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
